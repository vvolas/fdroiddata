AntiFeatures:UpstreamNonFree
Categories:Sports & Health
License:GPLv3
Web Site:https://glucosio.org/
Source Code:https://github.com/Glucosio/android
Issue Tracker:https://github.com/Glucosio/android/issues

Auto Name:Glucosio
Summary:Manage your diabetes
Description:
Manage and research diabetes.
.

Repo Type:git
Repo:https://github.com/Glucosio/android

Build:1.0,1
    commit=3391956e77b391e4e759ad4b607dad7a3e68c8c6
    subdir=app
    gradle=yes

Build:0.8.1,2
    commit=540384a2acda7421a344f686258758c4898900d9
    subdir=app
    gradle=yes

Build:0.8.2,3
    disable=play-services
    commit=0.8.2
    subdir=app
    gradle=yes

Build:0.10.3,18
    disable=play-services
    commit=0.10.3
    subdir=app
    gradle=yes

Build:1.1.2-FOSS,33
    disable=builds,wait for upstream and testing
    commit=197a9a65e28dc75149b53c30dfb4b27a9dcd6ce3
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/google-services/d' build.gradle ../build.gradle && \
        sed -i -e 's|@integer/google_play_services_version|0|g' src/main/AndroidManifest.xml && \
        sed -i -e 's/R.string.common_google_play_services_unsupported_text/"unsupported"/g' src/main/java/org/glucosio/android/activity/MainActivity.java

Maintainer Notes:
See https://github.com/Glucosio/android/issues/122
.

Archive Policy:0 versions
Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1.2
Current Version Code:33
